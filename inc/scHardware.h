/*!
 *  @file 	   scHardware.h
 *  @brief     Screen Cycle hardware header file.
 *  @details   This header file is used by scCycle.c for
 *  		   Macros used by scCycle.c should be added here. Also edit as needed.
 *
 *	@warning   It needs to include a valid LCD application header file.
 *  @author    Federico Baigorria
 *  @date      06-03-2020
 *  @copyright GNU Public License.
 */

#ifndef __INC_SCHARDWARE_H__
#define __INC_SCHARDWARE_H__

#include "FreeRTOS.h"
#include "semphr.h"

//#define SC_SWITCH_SCREEN xSemaphoreTake(semSW1press, 0)

#endif /* __INC_SCHARDWARE_H__ */
