/*!
 *  @file 	   scCycle.h
 *  @brief     Screen Cycle application header file.
 *  @details   Include this header in your application for screen control.
 *  		   Application macros used for Screen Cycle should be added here and edited as needed.
 *
 *	@warning   Screen Cycle must be edited per aplication requirement. Private (static) functions should
 *			   not be edited.
 *  @author    Federico Baigorria
 *  @date      06-03-2020
 *  @copyright GNU Public License.
 */

#ifndef __INC_SCREENCYCLE_H__
#define __INC_SCREENCYCLE_H__

///! User defined configuration
#include "scTypes.h"

///! Public task function
void scScreenTask(void);
void scInitAppStack(void);

#endif /* __INC_SCREENCYCLE_H__ */
