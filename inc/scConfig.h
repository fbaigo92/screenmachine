/*!
 *  @file 	   scConfig.h
 *  @brief     Screen Cycle application header file.
 *  @details   This header file is used by scCycle.c
 *  		   Macros used by scCycle.c should be added here. Also edit as needed.
 *
 *	@warning   It needs to include a valid LCD application header file.
 *  @author    Federico Baigorria
 *  @date      06-03-2020
 *  @copyright GNU Public License.
 */

#ifndef __INC_SCCONFIG_H__
#define __INC_SCCONFIG_H__

/** EDIT
 */

///! PV title for scVariable() function
#define SC_PV_TITLE				" EC: "
///! Whole part number format
#define SC_PV_WHOLE_DIGITS		5
///! Decimal part number format. Enable first
#define SC_PV_USE_DECIMALS		0
#define SC_PV_DECIMAL_DIGITS	2

///! Self header files
#include "stdint.h"
#include "math.h"
///! LCD module
//#include "../../../modules/lpc1769/qc12864b/inc/qcLCD.h" 	///! QC128x64
#include "../../../modules/lpc1769/ssd1306/inc/ssdOLED.h"	///! SSD1306
///! Hardware header files
#include "../../../modules/lpc1769/screenCycle/inc/scHardware.h"
#include "../../../modules/lpc1769/screenCycle/inc/scTypes.h"
#include "../../../projects/TDS/inc/switches.h"
#include "../../../projects/TDS/inc/ADS1115.h"
///! Flash memory
#include "../../../projects/TDS/inc/Memoria.h"

///! Clear line macros for scClean() using QC128x64
//#define CLEARF2 				qcSetText("                 ", QC_ROW2_BASE)
//#define CLEARF3 				qcSetText("                 ", QC_ROW3_BASE)
//#define CLEARF4 				qcSetText("                 ", QC_ROW4_BASE)
///! Clear line macros for scClean() using SSD1306
#define CLEARF2 				ssdOledCleanPage(1)
#define CLEARF3 				ssdOledCleanPage(2)
#define CLEARF4 				ssdOledCleanPage(3)

/** DO NOT EDIT
 */

///! Screens
typedef enum {
	scFirstScreen = 0,
	scSecondScreen,
	scThirdScreen,
	scFourthScreen,
	scFithScreen,
	scLastScreen
} scScreens;

///! Private functions
static void scClean(void);
static void scStrCpy(char *, char *);
static void scItoa(int, char *, uint8_t);
static void scFormatPV(char *, float, char *);
static void scUpdateAppStack(void *);
static void scUpdateAppFlash(void);
static void scUpdateOthers(void);
static uint8_t scUpdateBottons(uint8_t);

///! Screen functions (private)
static void scVariable(void *);
static void scMinMax(void *);
static void scKSetup(void *);
static void scOffsetSetup(void *);
static void scDatalog(void *);
static void scCalMenu(void *);
static void scCalRoutine(void *);
#endif /* __INC_SCCONFIG_H__ */
