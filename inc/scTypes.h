/*!
 *  @file 	   scTypes.h
 *  @brief     Screen Cycle application  data types header file.
 *  @details   This header file is used by scCycle.c and the proper application
 *  		   All data types used by scCycle.c functions should be added here. Also edit as needed.
 *
 *  @author    Federico Baigorria
 *  @date      06-03-2020
 *  @copyright GNU Public License.
 */

#ifndef __INC_SCTYPES_H__
#define __INC_SCTYPES_H__

///! Constant defines
#define SC_TRUE 			1
#define SC_FALSE			!SC_TRUE
#define SC_UPDT_CLEAN		0x01
#define SC_UPDT_LOGGING		0x02
#define	SC_UPDT_FLASH		0x04

#define SC_DFT_LOG_INT		15
///! Keys
#define	APP_KEY1_OP			1
#define	APP_KEY2_ADD		2
#define APP_KEY3_DEC		3

///! Struct data for datalogging Tasks
typedef struct {
	uint8_t isLogging;
	uint8_t interval;
	uint8_t pVarBuffer[10];
	uint8_t pMinBuffer[10];
	uint8_t pMaxBuffer[10];
} scDatalogCfg;

///! Struct for measurement task
typedef struct{
	float alfaFactor;
	float betaFactor;
} scADCTransform;

///! Application struct memory data
typedef struct {
	float 	 tdsOffset;			///! Offset
	float 	 tdsKCell;			///! Cell constant (K)
	float	 tdsAlfa;			///! Best fit curve constant Alfa (Nepper log)
	float 	 tdsBeta;			///! Best fit curve constant Beta (Nepper log)
	uint32_t tdsFlags;			///! Flags: Calibration done, RTC set, etc.
	uint32_t tdsLogInterval;	///! Datalogging interval
	uint32_t tdsSerial;			///! Serial Number
	uint32_t tdsCRC;			///! CRC verification (TODO: CRC generation and verification)
} appTDSMemStack;

///! Padding structure for writing memory flash (256 bytes). Change if needed
typedef struct {
	appTDSMemStack tdsMemory;
	uint32_t padding[56];
} appMemFlashStack;

///! Application struct data
typedef struct {
	uint8_t isLogging;
	float	tdsMin;
	float   tdsMax;
	float	tdsPvEc;
	float	tdsPvTDS;
	appTDSMemStack tdsMem;
} appMainStack;

///! Maximum PV value for PV percentage calculus
#define APP_PV_MAX_VAL	4400

#endif /* __INC_SCTYPES_H__ */
