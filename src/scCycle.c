/*!
 *  @file 	   screenCycle.c
 *  @brief     State machine for cycling different program screens
 *  @details   Screen cycle includes:
 *  		   PV (Process variable) display
 *  		   Minimun / Maximum recorded
 *			   Offset correction
 *			   Calibration config
 *			   Clock time config
 *  @author    Federico Baigorria
 *  @version   0.1.0
 *  @date      06-02-2020
 *  @copyright GNU Public License.
 */

#include "../../../modules/lpc1769/screenCycle/inc/scConfig.h"

#define APP_CAL_SET		0x04
#define APP_RTC_SET		0x02
#define	APP_MEM_SET		0x01

#define	APP_ACT_NULL	0x00
#define APP_ACT_PKEY	0x01
#define	APP_ACT_ADD		0x02
#define APP_ACT_DEC		0x03

#define APP_MAX_CAL_POINTS	2

///! Task handle for SD Logging task
xTaskHandle pxSDLog = NULL;
///! SD card logging data
xQueueHandle sdQueueLogData = NULL;

///! Screen control cycler
void (*scCtrlf[])(void*) = {
		&scVariable,
		&scMinMax,
		&scDatalog,
		&scCalMenu,
		&scCalRoutine
};
///! First time showing screen (clean it)
static uint8_t scDoUpdate = SC_UPDT_CLEAN;
///! Screen index
static uint8_t scIndex = 0;
///! Calibration point index
static uint8_t scCalPoint = 1;
///! Application struct data
static appMainStack appTDS;


/** Private functions
 */

/**
@fn static voic scClean(void)
@detail Clean lines when switching from screen to screen once..

@return void
*/
static void scClean(void){
	CLEARF2;
	CLEARF3;
	CLEARF4;
}

/**
@fn static void scStrCpy(char *scDestiny, char *scSource)
@detail String copy for Screen Cycle. It's null terminated.

@warning No null terminated character control is applied for destiny string (scDestiny) so user must assure
		 that enough space is allocated for it.

@param *scDestiny pointer to formatted string. scSource text will be added to it.
@param *scSource pointer to the text to be appended.
@return void
*/
static void scStrCpy(char *scDestiny, char *scSource){
	uint8_t scOffset = 0;

	while(*(scSource + scOffset) != '\0'){
		*(scDestiny + scOffset) = *(scSource + scOffset);
		scOffset++;
	}

	*(scDestiny + scOffset) = '\0';
}

/**
@fn static void scItoa(int scBase, char *scBuff, uint8_t isDecimal)
@detail Itoa for Screen Cycle. Base it's always decimal.

@warning No null terminated character control is applied for destiny string (scBuff) so user must assure
		 that enough space is allocated for it.

@param scBase Integer to be converted.
@param *scBuff Buffer to be allocated the ASCII text. It's null terminated.
@param isDecimal It's a flag for converting a whole or decimal part.
@return void
*/
static void scItoa(int scBase, char *scBuff, uint8_t isDecimal){
	uint8_t scRound;
	uint8_t scDigitCount = (isDecimal == SC_TRUE) ? SC_PV_DECIMAL_DIGITS - 1: SC_PV_WHOLE_DIGITS;
	int scAux;

	///! Convert whole digits to ASCII values
	for(scRound = 0; scRound < scDigitCount; scRound++){
		scAux = scBase % 10;
		*(scBuff + scDigitCount - scRound) = 0x30 + scAux;
		scBase = (scBase - scAux) / 10;
	}
	///! Leading zero, it's shown empty only for the whole part
	if(isDecimal == !SC_TRUE){
		*(scBuff + scDigitCount - scRound) = ' ';
	} else {
		scAux = scBase % 10;
		*(scBuff + scDigitCount - scRound) = 0x30 + scAux;
	}
	///! Null terminated
	scRound++;
	*(scBuff + scRound) = '\0';
}

/**
@fn static void scFormatPV(char *scBuff, float scPv, char *scText)
@detail Convert PV float value to an ASCII string, it can also be appended an aditional text such as "unit".

@warning For safe usage it's recommended that if decimals are used then the maximum SC_PV_WHOLE_DIGITS should be set
		 to 5 but if decimals aren't used then it can be up to 10. For decimals the maximum SC_PV_DECIMAL_DIGITS is
		 set as 5.

@param *scBuff pointer to ascii string.
@param row start position of said row
@return void
*/
static void scFormatPV(char *scBuff, float scPv, char *scText)
{
	int scWpart;
	uint8_t scOffset;
	///! Whole number portion. Limited to SC_PV_WHOLE_DIGITS digits
	scWpart = ((int)scPv) % (int)(pow(10, SC_PV_WHOLE_DIGITS));
	///! Converts whole part to text
	scItoa(scWpart, scBuff, !SC_TRUE);
	///! Offset for additional text (if no decimal part is added)
	scOffset = SC_PV_WHOLE_DIGITS + 1;

#if SC_PV_USE_DECIMALS
	int scDpart;
	///! Decimal number portion. Limited to SC_PV_DECIMAL_DIGITS digits
	scDpart = (int)((scPv - scWpart) * pow(10, SC_PV_DECIMAL_DIGITS));
	///! Set the decimal point
	*(scBuff + scOffset) = '.';
	///! Offset for decimal part
	scOffset++;
	///! Converts decimal part to text
	scItoa(scDpart, (scBuff + scOffset), SC_TRUE);
	///! Offset for additional test (if no decimal part is added)
	scOffset += SC_PV_DECIMAL_DIGITS;
#endif

	///! Append additional text
	if(scText){
		scStrCpy((scBuff + scOffset), scText);
	}
}


/** Public functions
 */

/**
@fn static void scFormatPV(char *scBuff, float scPv, char *scText)
@detail Convert PV float value to an ASCII string, it can also be appended an aditional text such as "unit".

@warning For safe usage it's recommended that if decimals are used then the maximum SC_PV_WHOLE_DIGITS should be set
		 to 5 but if decimals aren't used then it can be up to 10. For decimals the maximum SC_PV_DECIMAL_DIGITS is
		 set as 5.

@param *scBuff pointer to ascii string.
@param row start position of said row
@return void
*/
static void scVariable(void *scPv){
	char buffer[15];
	uint8_t percent= (float) (( appTDS.tdsPvEc / APP_PV_MAX_VAL) * 100.0);

	///! Screen cleaning
	if(scDoUpdate & SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;
		ssdOledBarGraph();	///! New bar graph
	}
	///! Format PV variable
	scFormatPV(buffer, appTDS.tdsPvEc, (char*) " uS");
	///! Showing PV variable
	ssdOledSetPageOrigin(1);
	ssdOledSetText(SC_PV_TITLE, 1);
	ssdOledSetText(buffer, 1);
	///! Show PV variable in bar graph
	ssdOledUpdateBar(percent);
	///! Update APP's stack flash
	if(scDoUpdate & SC_UPDT_FLASH){
		scDoUpdate &= ~SC_UPDT_FLASH;

		portENTER_CRITICAL();
		scUpdateAppFlash();
		portEXIT_CRITICAL();
	}
	vTaskDelay(1000/portTICK_RATE_MS);
}

static void scMinMax(void *scPtr){
	char buffer[15];
	float pVmin = appTDS.tdsMin;
	float pVmax = appTDS.tdsMax;

	///! Screen cleaning
	if(scDoUpdate == SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;
	}
	///! Showing minimum recorded value
	scFormatPV(buffer, pVmin, (char*) " uS");
	ssdOledSetPageOrigin(1);
	ssdOledSetText(" MIN: ", 1);
	ssdOledSetText(buffer, 1);
	///! Showing maximum recorded value
	scFormatPV(buffer, pVmax, (char*) " uS");
	ssdOledSetPageOrigin(2);
	ssdOledSetText(" MAX: ", 1);
	ssdOledSetText(buffer, 1);
	vTaskDelay(100/portTICK_RATE_MS);
}

///! Not used
static void scKSetup(void *scPtr){
	uint8_t doAddDec = *((uint8_t *)scPtr);
	char buffer[15];

	///! Screen cleaning
	if(scDoUpdate == SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;

		scFormatPV(buffer, appTDS.tdsMem.tdsKCell, 0);
		ssdOledSetPageOrigin(1);
		ssdOledSetText(" Cell K: ", 1);
		ssdOledSetText(buffer, 1);
	}
	///! Increment
	if(doAddDec == APP_ACT_ADD){
		appTDS.tdsMem.tdsKCell += 0.1;
		if(appTDS.tdsMem.tdsKCell > 10.0)
			appTDS.tdsMem.tdsKCell = 10.0;
	}
	///! Decrement
	if(doAddDec == APP_ACT_DEC){
		appTDS.tdsMem.tdsKCell -= 0.1;
		if(appTDS.tdsMem.tdsKCell < 0.0)
			appTDS.tdsMem.tdsKCell = 0.0;
	}
	///! Update legend
	if(doAddDec != APP_ACT_NULL){
		scFormatPV(buffer, appTDS.tdsMem.tdsKCell, 0);
		ssdOledSetPageOrigin(1);
		ssdOledSetText(" Cell K: ", 1);
		ssdOledSetText(buffer, 1);
	}
}

///! Not used
static void scOffsetSetup(void *scPtr){
	uint8_t doAddDec = *((uint8_t *)scPtr);
	char buffer[15];

	///! Screen cleaning
	if(scDoUpdate == SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;

		scFormatPV(buffer, appTDS.tdsMem.tdsOffset, " uS");
		ssdOledSetPageOrigin(1);
		ssdOledSetText(" Offset: ", 1);
		ssdOledSetText(buffer, 1);
	}
	///! Increment
	if(doAddDec == APP_ACT_ADD){
		appTDS.tdsMem.tdsOffset += 0.1;
		if(appTDS.tdsMem.tdsOffset > 5.0)
			appTDS.tdsMem.tdsOffset = 5.0;
	}
	///! Decrement
	if(doAddDec == APP_ACT_DEC){
		appTDS.tdsMem.tdsOffset -= 0.1;
		if(appTDS.tdsMem.tdsOffset < -5.0)
			appTDS.tdsMem.tdsOffset = -5.0;
	}
	///! Update legend
	if(doAddDec != APP_ACT_NULL){
		scFormatPV(buffer, appTDS.tdsMem.tdsOffset, " uS");
		ssdOledSetPageOrigin(1);
		ssdOledSetText(" Offset: ", 1);
		ssdOledSetText(buffer, 1);
	}
}

static void scDatalog(void *scPtr){
	uint8_t doAddDec = *((uint8_t *)scPtr);
	char buffer[15];

	///! Screen cleaning
	if(scDoUpdate == SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;
	}
	///! Logging legend
	ssdOledSetPageOrigin(1);
	if(appTDS.isLogging)
		ssdOledSetText(" Logging:  Yes", 1);
	else
		ssdOledSetText(" Logging:   No", 1);

	///! Interval
	ssdOledSetPageOrigin(2);
	ssdOledSetText(" Interval: ", 1);
	scFormatPV(buffer, (float)appTDS.tdsMem.tdsLogInterval, " s");
	ssdOledSetText(buffer, 1);
	///! Change logging status
	if(doAddDec == APP_ACT_ADD){
		appTDS.isLogging = (~ appTDS.isLogging) & 0x0001;
		scDoUpdate |= SC_UPDT_LOGGING;
	}
	///! Change interval
	if(doAddDec == APP_ACT_DEC){
		appTDS.tdsMem.tdsLogInterval += 5;
		if(appTDS.tdsMem.tdsLogInterval > 60)	///! TODO: Add define
			appTDS.tdsMem.tdsLogInterval = 15;	///! TODO: Add default define
	}
}

static void scCalMenu(void *scPtr){
	uint8_t doAddDec = *((uint8_t *)scPtr);

	///! Screen cleaning
	if(scDoUpdate == SC_UPDT_CLEAN){
		scClean();
		scDoUpdate &= ~SC_UPDT_CLEAN;

		///! Calibration setting
		ssdOledSetPageOrigin(1);
		if(appTDS.tdsMem.tdsFlags & APP_CAL_SET)
			ssdOledSetText(" Calibrated: Yes", 1);
		else
			ssdOledSetText(" Calibrated:  No", 1);

		ssdOledSetPageOrigin(2);
		ssdOledSetText(" RTC Disabled", 1);
	}

	///! Change Calibration status
	if(doAddDec == APP_ACT_ADD){
		appTDS.tdsMem.tdsFlags ^= APP_CAL_SET;

		///! Calibration setting
		ssdOledSetPageOrigin(1);
		if(appTDS.tdsMem.tdsFlags & APP_CAL_SET)
			ssdOledSetText(" Calibrated: Yes", 1);
		else
			ssdOledSetText(" Calibrated:  No", 1);
	}
}

static void scCalRoutine(void *scPtr){
	uint8_t doAddDec = *((uint8_t *)scPtr);
	scADCTransform scCalFactors;

	if(!(appTDS.tdsMem.tdsFlags & APP_CAL_SET)){
		///! Screen cleaning
		if(scDoUpdate == SC_UPDT_CLEAN){
			scClean();
			scDoUpdate &= ~SC_UPDT_CLEAN;

			///! Calibration legend
			ssdOledSetPageOrigin(1);
			ssdOledSetText(" Calibration", 1);
		}

		///! Set new calibration point
		if(doAddDec == APP_ACT_ADD){
			switch(scCalPoint){
				case 1:
					///! Calibration legend
					ssdOledSetPageOrigin(1);
					ssdOledSetText(" 1st cal. point set", 1);
					xSemaphoreGive(adsCalibrate);
					break;

				case 2:
					ssdOledSetPageOrigin(1);
					ssdOledSetText(" 2nd cal. point set", 1);
					xSemaphoreGive(adsCalibrate);
					break;

				default:
					scCalPoint = 0;
					scIndex = scFirstScreen;
					scDoUpdate |= SC_UPDT_CLEAN | SC_UPDT_FLASH;
					appTDS.tdsMem.tdsFlags |= APP_CAL_SET;
					xSemaphoreGive(adsCalibrate);
					xQueueReceive(adsCalPoints, &scCalFactors, portMAX_DELAY);
					appTDS.tdsMem.tdsAlfa = scCalFactors.alfaFactor;
					appTDS.tdsMem.tdsBeta = scCalFactors.betaFactor;
					break;
			}
			scCalPoint++;
		}
	} else {
		scIndex = scFirstScreen;
		scDoUpdate |= SC_UPDT_CLEAN;
	}
}

///! Screen task operations (End of screen definitions)

static void scUpdateAppStack(void *scPv){
	///! Update PV variable in the app data stack
	appTDS.tdsPvEc = *((float*)scPv);
	///! New maxium
	if(appTDS.tdsPvEc > appTDS.tdsMax)
		appTDS.tdsMax = appTDS.tdsPvEc;
	///! New minimum
	if(appTDS.tdsPvEc < appTDS.tdsMin)
		appTDS.tdsMin = appTDS.tdsPvEc;
}

static void scUpdateOthers(void){
	scDatalogCfg sdDataLog;

	sdDataLog.interval = appTDS.tdsMem.tdsLogInterval;
	sdDataLog.isLogging = appTDS.isLogging;

	///! vTaskSDMLogging logging task update with the appStack data
	if(appTDS.isLogging){
		scFormatPV(sdDataLog.pVarBuffer, appTDS.tdsPvEc, (char*) " uS");
		scFormatPV(sdDataLog.pMinBuffer, appTDS.tdsMin, (char*) " uS");
		scFormatPV(sdDataLog.pMaxBuffer, appTDS.tdsMax, (char*) " uS");
	}
	///! Do once each time the logging enabling is set
	if(scDoUpdate == SC_UPDT_LOGGING){
		scDoUpdate &= ~SC_UPDT_LOGGING;
		if(appTDS.isLogging){
			vTaskResume(pxSDLog);
		} else {
			vTaskSuspend(pxSDLog);
		}
	}
	///! Send new updated data for the Logging and Monitoring tasks
	xQueueOverwrite(sdQueueLogData, &sdDataLog);
}

static uint8_t scUpdateBottons(uint8_t scKeyStatus){
	uint8_t scKeyOption = APP_ACT_NULL;

	if(scKeyStatus == APP_KEY1_OP){
		scKeyOption |= APP_ACT_PKEY;
	}
	if(scKeyStatus == APP_KEY2_ADD){
		scKeyOption |= APP_ACT_ADD;
	}
	if(scKeyStatus == APP_KEY3_DEC){
		scKeyOption |= APP_ACT_DEC;
	}
	return scKeyOption;
}

static void scUpdateAppFlash(void){
	appMemFlashStack *appFlashStack = NULL;
	appFlashStack = (appMemFlashStack*) pvPortMalloc(sizeof(appMemFlashStack));

	if(appFlashStack){
		appFlashStack->tdsMemory = appTDS.tdsMem;
		writeFlash(appFlashStack, sizeof(appMemFlashStack) / sizeof(uint8_t));
	}
	vPortFree(appFlashStack);
}

void scInitAppStack(void){
	appMemFlashStack *appFlashStack = NULL;
	appTDSMemStack *stackAux = NULL;
	scADCTransform factorTable;

	stackAux = (appTDSMemStack *) readFlash();
	appFlashStack = (appMemFlashStack*) pvPortMalloc(sizeof(appMemFlashStack));

	if(stackAux){
		appTDS.tdsMem = *stackAux;

		if(!((appTDS.tdsMem.tdsFlags & APP_MEM_SET) == APP_MEM_SET)){
			appTDS.isLogging = SC_FALSE;
			appTDS.tdsMem.tdsLogInterval = SC_DFT_LOG_INT;
			appTDS.tdsMem.tdsOffset = 0.0;
			appTDS.tdsMem.tdsKCell = 1.0;
			appTDS.tdsMem.tdsAlfa = -945.8;
			appTDS.tdsMem.tdsBeta = 9253.9;
			appTDS.tdsMem.tdsSerial = 20200511;
			appTDS.tdsMem.tdsFlags = APP_MEM_SET | APP_CAL_SET | ~APP_RTC_SET;
			appFlashStack->tdsMemory = appTDS.tdsMem;

			writeFlash(appFlashStack, sizeof(appMemFlashStack) / sizeof(uint8_t));
		}
	}
	vPortFree(appFlashStack);

	///! Send factors for counts to PV convertion for the ADC task
	factorTable.alfaFactor = appTDS.tdsMem.tdsAlfa;
	factorTable.betaFactor = appTDS.tdsMem.tdsBeta;
	xQueueSend(adsCalPoints, &factorTable, 0);
}

void scScreenTask(void){
	float appPv;
	///! Update buttons status
	uint8_t scKeyPressed = Leer_Pulsadores();
	uint8_t doAddDec = APP_ACT_NULL;
	///! Update stack info
	if(uxQueueMessagesWaiting(adsQueue) > 0){
			xQueueReceive(adsQueue, &appPv, portMAX_DELAY);
			scUpdateAppStack(&appPv);
	}
	///! Update queues for other modules
	scUpdateOthers();

	///! Show indexed screen
	switch(scIndex){
		case scFirstScreen:
			(*scCtrlf[scIndex])(0);
			break;

		case scSecondScreen:
			(*scCtrlf[scIndex])(0);
			break;

		case scThirdScreen:
			doAddDec = scUpdateBottons(scKeyPressed);
			(*scCtrlf[scIndex])(&doAddDec);
			break;

		case scFourthScreen:
			doAddDec = scUpdateBottons(scKeyPressed);
			(*scCtrlf[scIndex])(&doAddDec);
			break;

		case scFithScreen:
			doAddDec = scUpdateBottons(scKeyPressed);
			(*scCtrlf[scIndex])(&doAddDec);
			break;

		default:
			break;
	}

	if(scKeyPressed == APP_KEY1_OP && scIndex != scFithScreen){
		scIndex = (scIndex + 1) % scLastScreen;
		scDoUpdate |= SC_UPDT_CLEAN;
	}
}
